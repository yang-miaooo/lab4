package com.example.task4;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
    private int count=0;
    private boolean quit=false;
    private MyServiceBinder myServiceBinder=new MyServiceBinder();
    public MyService() {
    }
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("Myservice","Service is binded");//log显示绑定
        return myServiceBinder;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(!quit){
                    try{
                        Thread.sleep(1000);//暂停1000毫秒 也就是1秒
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    MyService.this.count++;
                }
            }
        }).start();
//创建新 Thread，如果 service 启动每秒钟 count++，如果 quit 为真则退出
    }
    class MyServiceBinder extends Binder {
        public int getCount(){
            return MyService.this.count;
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        quit=true;
    }
}
